﻿using System.ServiceProcess;
using System.Timers;

namespace Service_Test {
    public partial class Service1 : ServiceBase {

        public Service1() {
            InitializeComponent();
        }

        protected override void OnStart(string[] args) { //-------------------- add comments towards functionality

            //Application Code
            //Library.fireLight();
            Timer aTimer = new System.Timers.Timer();

            try {

                if(Library.checkIODevice() == 1) {
                    
                    System.IO.Directory.CreateDirectory(Library.path); //Create error log directory
                    Library.loadSettingsFile(); //Load variables from settings.xml file
                    if (Library.GisConnect() == 1) {
                        Library.lightStackFun(); //visible method of connection status

                        //Check input status every 300ms
                        aTimer.Elapsed += new ElapsedEventHandler(Library.checkInputEvent);
                        aTimer.Interval = 300;
                        aTimer.Enabled = true; //------------------test
                    }//Connect to Gis Device
                    else {
                        Library.WriteLog("Could not establish GIS mbH Connection", 
                            System.Reflection.MethodBase.GetCurrentMethod().Name);
                        Stop();
                    } 
                }
                else {
                    Library.WriteLog("Could not establish AccessIO Connection", 
                        System.Reflection.MethodBase.GetCurrentMethod().Name);
                    Stop();
                }
            } catch {
                Library.WriteLog("Failed to Start", 
                    System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
        }

        protected override void OnStop() {
            Library.WriteLog("Service Stopped", 
                System.Reflection.MethodBase.GetCurrentMethod().Name);
            Library.lightStackFun();
            Library.setAllOutputsLow();

        }
    }
}
