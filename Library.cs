﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using GiS_Commands;
using System.IO.Ports;
using AIOUSBNet;
using System.Globalization;
using System.Xml;
using System.IO;
using System.ServiceProcess;
using System.Diagnostics;

namespace Service_Test {
    public static class Library {

        //change "process' to actual functionality
        //define better flow + regional components
        //connection status system tray
        //try catch good tag
        //try catch!!

        private static UInt32 DeviceIndex;
        private static System.Timers.Timer aTimer = new System.Timers.Timer();

        //GiS Variables
        private static int intHandle;
        private static sbyte[] NameList = new sbyte[255];

        //---Settings.xml Variables---
        private static string LFReaderPort;
        private static int hornDuration;
        private static string suffixCharacters;
        
        //Serial Port Variables
        private static string printerPort;
        private static int baudRate;
        private static Parity parityP;
        private static int dataBits;
        private static StopBits stopBitsS;

        public static bool connected = true;

        //Application Log Location
        public static string path = "c:\\eAgile\\RFID Scan-To-Print";
        private const string STR_SERVICE = "Scan-To-Print";

        //Boolean Condition Values for Reader Time Constraint
        private static string previousValue = "";
        private static Boolean failState = false;
        private static List<char> settingsVar = new List<char>();

        public static void loadSettingsFile() {

            try {
                XmlDocument settings = new XmlDocument();

                settings.Load(path + "\\settings.xml");

                XmlNodeList elemList = settings.GetElementsByTagName("variable");
                LFReaderPort = elemList[0].InnerXml;
                printerPort = elemList[1].InnerXml;
                hornDuration = Int32.Parse(elemList[2].InnerXml);
                suffixCharacters = elemList[3].InnerXml;
                baudRate = Int32.Parse(elemList[4].InnerXml);
                string parityTest = (elemList[5].InnerXml);
                dataBits = Int32.Parse(elemList[6].InnerXml);
                string stopBits = (elemList[7].InnerXml);

                parityP = (Parity) Enum.Parse(typeof(Parity), parityTest, true);
                stopBitsS = (StopBits) Enum.Parse(typeof(StopBits), stopBits, true);
            } catch (Exception ex) {
                WriteLog(ex.ToString(), System.Reflection.MethodInfo.GetCurrentMethod().Name);
                Environment.Exit(0); //exit application if file not found
            }
        }

        public static int GisConnect() {

            int intResponse, intI;
            string conStringGiS, strTemp;

            try {

                conStringGiS = "\\\\.\\" + LFReaderPort + " [GiS Virtual COM]";
                intI = 0;

                foreach (char c in conStringGiS) {
                    strTemp = Convert.ToInt32(c).ToString();
                    NameList[intI] = Convert.ToSByte(strTemp);
                    intI++;
                }

                intHandle = GIS_LF_API.TSLF_Open(NameList, 19200, 0, 1000);
                if (intHandle > 0) {
                    intResponse = GIS_LF_API.TSLF_SetIO(intHandle, 224, 0);
                    if (intResponse == 0) {
                        intResponse = GIS_LF_API.TSLF_SetIO(intHandle, 224, 64);  //Green LED
                    }
                    return 1;
                } else {
                    return 0;
                }

            } catch (Exception ex) {
                WriteLog(ex.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name);
                return 0;
            }
        }

        public static void checkInputEvent(object source, ElapsedEventArgs e) {

            UInt32 result;
            aTimer.Enabled = false;
            byte[] bits = new byte[4];

            try {
                if (!failState) {
                    //check io device connection status
                    if (AIOUSB.DIO_ReadAll(DeviceIndex, bits) == 0) {
                        //int intResponse = GIS_LF_API.TSLF_SetIO(intHandle, 224, 64);  //Green LED  
                        //check tag-read device connection status
                        if (GIS_LF_API.TSLF_SetIO(intHandle, 224, 64) == 0) {
                            string txtCheck = ByteArrayToString(bits); //try catch opportunity to writelog if string convert failure
                            /*
                            * Code below will only run when there is a change in the input values
                            *     from any value, to that representing both triggers down. This 
                            *     eliminates to the need to handle timing / memory access exceptions.
                            *     
                            * failState is a boolean variable used to hold the checkInputEvent
                            *     until the failure process has completed.  
                            */
                            if (!txtCheck.Equals(previousValue)) {
                                failState = true;
                                previousValue = txtCheck;
                                if (txtCheck.Substring(5, 1).Equals("3")) { //Operator Presses 2 I/O Buttons
                                    setAllOutputsLow();
                                    string tagData = readTag(); //Application Reads RFID Tag Value
                                    if (!tagData.Equals("Fail")) {
                                        goodTag(tagData); //On Success
                                    } else { badTag(); } //On Failure                 
                                    ResetDevice(); // GiS device reset
                                }
                                aTimer.Enabled = true;
                                failState = false;
                            }
                        } else { // if non-responsive tag read
                            WriteLog("GiS mbH Disconnected", System.Reflection.MethodBase.GetCurrentMethod().Name);
                            setAllOutputsLow();
                        }
                    } else { //if on-responsive IO set
                        WriteLog("AccessIO Disconnected", System.Reflection.MethodBase.GetCurrentMethod().Name);
                    }
                }
               
            }
            catch(Exception ex) {
                WriteLog(ex.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
            
           
        }

        private static string ByteArrayToString(byte[] ba) {
            string hex = BitConverter.ToString(ba);
            return hex.Replace("-", "");
        }

        public static void setAllOutputsLow() {
            UInt32 bits = 0;
            UInt32.TryParse("FFFFFFFF", NumberStyles.HexNumber, null as IFormatProvider, out bits);
            AIOUSB.DIO_WriteAll(DeviceIndex, ref bits);
        }

        private static string readTag() {
            byte[] bytRead = new byte[4];
            int intResponse;
            string strWrite;
            List<string> tagList = new List<string>();
            List<string> checkSumList = new List<string>();

            try {
                for (int i = 0; i < 3; i++) {
                    intResponse = GIS_LF_API.TSLF_Read(intHandle, 9, 1, bytRead, 4);
                    if (intResponse > 0) {
                        strWrite = FormatResult(bytRead);
                        tagList.Add(strWrite);
                        checkSumList.Add(CalculateChecksum(strWrite));
                    } else { break; } //No tag found. Break.
                }
                if (tagList.Count == 3) {
                    if (checkList(tagList) && checkList(checkSumList)) { //check if any values differ from others
                        return tagList[0]; //good tags with same values & checksums
                    } else { return "Fail"; }
                } else { return "Fail"; }
            }
            catch(Exception ex) {
                WriteLog(ex.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name);
                return "Fail";
            }
        }

        private static void goodTag(string tagData) {
            AIOUSB.DIO_Write1(DeviceIndex, 1, 0);
            getSuffixCharacters(suffixCharacters);

            if (settingsVar.Count() == 2) {
                sendData(tagData + settingsVar[0] + settingsVar[1]);
                using (EventLog log = new EventLog("Application")) {
                    log.Source = STR_SERVICE;
                    log.WriteEntry("Tag Data: " + tagData, EventLogEntryType.Information, 1);
                }
            }
            if (settingsVar.Count() == 1) {
                sendData(tagData + settingsVar[0]);
                using (EventLog log = new EventLog("Application")) {
                    log.Source = STR_SERVICE;
                    log.WriteEntry("Tag Data: " + tagData, EventLogEntryType.Information, 1);
                }
            } else {
                WriteLog("Suffix Characters in Settings.xml not found: items in Settings var...:" +
                    settingsVar.Count(), System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
        }

        private static void badTag() {
            try {
               // failState = true; //Used to keep application out until process 6 is complete
                Console.WriteLine("Tag Failure");
                AIOUSB.DIO_Write1(DeviceIndex, 2, 0); //Output 2 high
                AIOUSB.DIO_Write1(DeviceIndex, 3, 0); //Output 3 high
                System.Threading.Thread.Sleep(hornDuration); //Sleep for x seconds
                AIOUSB.DIO_Write1(DeviceIndex, 3, 1); //Output 3 low
                //failState = false;
            }
            catch (Exception ex) {
                WriteLog(ex.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
            
        }

        private static string FormatResult(byte[] arr) {
            string strResult;
            Array.Reverse(arr);
            strResult = BitConverter.ToString(arr);
            strResult = strResult.Replace("-", "");
            return strResult;
        }

        private static string CalculateChecksum(string dataToCalculate) {
            byte[] byteToCalculate = Encoding.ASCII.GetBytes(dataToCalculate);
            int checksum = 0;
            foreach (byte chData in byteToCalculate) {
                checksum += chData;
            }
            checksum &= 0xff;
            return checksum.ToString("X2");
        }

        //Function used to check if all items in list are equal
        private static bool checkList(List<string> elements) {
            if (elements.Any(o => o != elements[0])) {
                return false;
            } else { return true; }
        }

        public static UInt32 checkIODevice() { //initial check used in main method
            return AIOUSB.GetDevices();
        }
        private static void getSuffixCharacters(string suffix) { //characters found in settings file

            //if settingsVar contains items
            if (settingsVar.Count() != 0) {
                settingsVar.Clear();
            }

            //add char variables from settings file
            if (suffix.Equals("CRLF")) {
                settingsVar.Add(Convert.ToChar(10));
                settingsVar.Add(Convert.ToChar(13));
            }
            else if (suffix.Equals("TAB")) {
                settingsVar.Add(Convert.ToChar(9));
            }
            else if (suffix.Equals("SPACE")) {
                settingsVar.Add(Convert.ToChar(32));
            }
            else if (suffix.Equals("ESC")) {
                settingsVar.Add(Convert.ToChar(27));
            }
        }

        private static char[] convertSuffix(string suffix) {
            String value = suffix;
            Char delimiter = ',';
            String[] substrings = value.Split(delimiter);
            foreach (var substring in substrings)
                Console.WriteLine(substring);

            char[] myChar = suffix.ToCharArray();
            return myChar;
        }
   
        private static void ResetDevice() {
            int intResponse;
            intResponse = GIS_LF_API.TSLF_ResetTransponder(intHandle, 9);
        }

        private static void sendData(string data) {

            try {
                SerialPort port = new SerialPort(
                            printerPort, baudRate, parityP, dataBits, stopBitsS);
                port.Open(); // Open the port for communications
                byte[] bytes = Encoding.ASCII.GetBytes(data); // Write bytes
                port.Write(bytes, 0, bytes.Length);
                System.Threading.Thread.Sleep(250); //possibly not required

                port.Close(); // Close the port
                Console.WriteLine("Tag Data Sent via: " + printerPort + " " + data);
            } catch (Exception ex) {
                WriteLog(ex.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name); //Sends the caught error within function x
            }
        }

        public static void WriteLog(string logMessage, string function) {
            try {
                using (StreamWriter w = File.AppendText("c:\\eAgile\\RFID Scan-To-Print\\log.txt")) {
                    if (function.Equals("OnStop")) {
                        w.Write(DateTime.Now + "   Warn ");
                    } else {
                        w.Write(DateTime.Now + "  Error ");
                    }
                    w.Write("[" + function + "]\t" + logMessage);
                    w.WriteLine();
                }
            }
            catch(Exception ex) {
                WriteLog(ex.ToString(), System.Reflection.MethodBase.GetCurrentMethod().Name);
            }
         }  

        public static void lightStackFun() { //Set Outputs High 3 seconds

            setAllOutputsLow();

            AIOUSB.DIO_Write1(DeviceIndex, 2, 0); //Output 2 high
            AIOUSB.DIO_Write1(DeviceIndex, 1, 0); //Output 
            System.Threading.Thread.Sleep(3000); //Sleep for x seconds
            AIOUSB.DIO_Write1(DeviceIndex, 2, 1); //Output 2 low
            AIOUSB.DIO_Write1(DeviceIndex, 1, 1); //Output 3 low
            
        }
    }
}
